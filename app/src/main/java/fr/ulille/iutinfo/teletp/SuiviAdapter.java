package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder>/* TODO Q6.a */ {

    // TODO Q6.a
    final SuiviViewModel model;
    private LayoutInflater mInflater;


    class ViewHolder extends RecyclerView.ViewHolder{
        TextView question ;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            question = (TextView) itemView.findViewById(R.id.question);
        }



    }
    public SuiviAdapter (Context context, SuiviViewModel model){
        this.model=model;
        model.initQuestions(context);
        this.mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public SuiviAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_view,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.question.setText(model.getQuestions(position));
    }



    @Override
    public int getItemCount() {
        return model.questions.length;
    }
    // TODO Q7
}
