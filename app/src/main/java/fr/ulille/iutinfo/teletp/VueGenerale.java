package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1

    private  String DISTANCIEL;
    private String salle;
    private String poste;
    // TODO Q2.c
    SuiviViewModel model ;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        String[] salles = getResources().getStringArray(R.array.list_salles);
        DISTANCIEL = salles[0];
        salle = DISTANCIEL;
        poste="";
        System.out.println("salle :"+DISTANCIEL);
        // TODO Q2.c
        model = new SuiviViewModel(getActivity().getApplication());
        // TODO Q4
        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> listeSalles = ArrayAdapter.createFromResource(this.getContext(),R.array.list_salles,android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> listePostes = ArrayAdapter.createFromResource(this.getContext(),R.array.list_postes,android.R.layout.simple_spinner_item);
        spSalle.setAdapter(listeSalles);
        spPoste.setAdapter(listePostes);
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            EditText login = (EditText) this.getActivity().findViewById(R.id.tvLogin);
            model.setUsername(login.getText()+"");
            Toast.makeText(this.getContext(),login.getText()+"",Toast.LENGTH_LONG).show();
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        update();
        // TODO Q9
    }

    // TODO Q5.a
    public void update () {
        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        if (spSalle.getSelectedItemId()==0){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation("Distanciel");
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(spSalle.getSelectedItem().toString()+" "+spPoste.getSelectedItem().toString());
        }
    }
    // TODO Q9
}